# sample-app-with-abac-spring-security
A sample spring boot application that uses abac-spring-security

1. ``$ git clone https://github.com/jferrater/abac-spring-security.git``
2. ``$ cd abac-spring-security && ./gradlew clean build publishToMavenLocal``
4. Clone this project and run
5. On the browser, http://localhost:8888/helloWorld where the username is Alice and password is password for logging in.
