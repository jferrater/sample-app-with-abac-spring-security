package com.github.joffryferrater.sampleappwithxacmlpepspringsecurity;

import static com.github.joffryferrater.sampleappwithxacmlpepspringsecurity.AuthorizationConstants.SECURED_PATH_ACCESS;

import com.github.joffryferrater.pep.security.AbacWebSecurityExpressionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private ApplicationContext applicationContext;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
            .formLogin().and()
            .authorizeRequests()
            .expressionHandler(webSecurityExpressionHandler())
            .antMatchers(HttpMethod.GET, "/securedPath").access(SECURED_PATH_ACCESS)
            .anyRequest().authenticated();

    }

    private AbacWebSecurityExpressionHandler webSecurityExpressionHandler() {
        return this.applicationContext.getBean(AbacWebSecurityExpressionHandler.class);
    }

}
