package com.github.joffryferrater.sampleappwithxacmlpepspringsecurity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(
	scanBasePackages={
		"com.github.joffryferrater.sampleappwithxacmlpepspringsecurity",
		"com.github.joffryferrater.pep" //Scans the abac-spring-security configurations
	})
public class SampleAppWithXacmlPepSpringSecurityApplication {

	public static void main(String[] args) {
		SpringApplication.run(SampleAppWithXacmlPepSpringSecurityApplication.class, args);
	}
}
