package com.github.joffryferrater.sampleappwithxacmlpepspringsecurity;

import static com.github.joffryferrater.sampleappwithxacmlpepspringsecurity.AuthorizationConstants.HELLOWORLD_ACCESS;
import static com.github.joffryferrater.sampleappwithxacmlpepspringsecurity.AuthorizationConstants.HELLOWORLD_ID_ACCESS;

import java.security.Principal;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SampleResource {

    @GetMapping("/helloWorld")
    @PreAuthorize(HELLOWORLD_ACCESS)
    public String printHelloWorld(Principal principal) {
        return "hello world";
    }

    @GetMapping("/helloWorld/{id}")
    @PreAuthorize(HELLOWORLD_ID_ACCESS)
    public String getHelloWorldId(@PathVariable String id) {
        return "hello world id is: " + id;
    }

    @GetMapping("/securedPath")
    public String getSecuredPath() {
        return "This is the /securedPath ";
    }
}
